/**
 * Common workflow in java:
 * 
 * Create an interface (let's us program in general)
 * Create an abstract class (let's us code shared functionality
 * Create concrete classes
 * 
 * Abstract:
 * -Cannot be instantiated
 * -Concrete classes that inherit from abstract class must implement all abstract methods
 * -Abstract subclasses may or may not implement the abstract methods
 * 
 * Implements means to inherit from an interface
 * You can implemeent as many interfaces as you want
 * @author unouser
 *
 */
public abstract class ACar implements ICar{
	/** The make of the car */
	private String make;
	
	/** The model of the car */
	private String model;
	
	/** The year of the car */
	private int year;
	
	/** The mileage of the car */
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		
		
		make = inMake;
		
		
		model = inModel;
		
		
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		return getMake() + " " + getModel();
	}
	
	
	
}
