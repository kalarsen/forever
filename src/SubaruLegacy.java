
public class SubaruLegacy extends ACar {

	public SubaruLegacy(int inYear, int inMileage)
	{
		super("Subaru", "Legacy", inYear);
		setMileage(inMileage);
	}
	
}
