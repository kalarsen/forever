/**
 * Different types in java:
 * class
 * enum
 * primitives
 * interfaces
 * 
 * A class is a combination of data and methods.
 * 
 * An interface only defines methods that it's subclasses will have.
 * By definition, everything in an interface is public.
 * @author unouser
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
